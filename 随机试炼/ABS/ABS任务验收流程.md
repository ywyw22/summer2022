# 提交ABS任务的PR，获得贡献值

通过ABS平台完成编译后，必须将编译成果提交PR，才可获得贡献值。

参考本文档的步骤提交编译任务的PR。

## 第一步：构建软件包，并获取软件包链接。

1. 按照<a href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E9%9A%8F%E6%9C%BA%E8%AF%95%E7%82%BC/ABS/ABS%E4%BB%BB%E5%8A%A1%E6%89%A7%E8%A1%8C%E6%B5%81%E7%A8%8B.md" target="_blank">构建软件包</a>中的步骤，确保已经构建了一个软件包。

    **注意：** 确保ABS系统中，对应的编译链接最终是**成功**状态。如果编译失败，可以重新尝试编译，或者联系我们。

2. 通过分享按钮得到软件包链接，以备后用。例如，`https://abs.openanolis.cn/all_project/package/5567`。

    ![image.png](./assets/abs-pr-001.png)


## 第二步：将任务成果提交PR

1. 准备PR。

   1.1 前往<a href="https://gitee.com/anolis-challenge/activity-box-lab" target="_blank">anolis-challenge/activity-box-lab仓库</a>下点击fork，把官方仓库的代码fork到你个人的gitee仓库中。

    ![](./assets/abs-pr-002.png)

    1.2. 在个人空间的activity-box-lab仓库的**ABS**文件夹下创建一个子文件夹，该文件夹以你的Gitee用户名来命名。注意是Gitee账号名，而不是昵称。

    ![image.png](./assets/abs-pr-003.png)

    1.3. 在该目录下创建一个扩展名为xxx.spec的文件。

    ![image.png](./assets/abs-pr-004.png)

    ![image.png](./assets/abs-pr-005.png)

    1.4. 在spec文件中输入你的编译成果。格式是`abs_url=xxxxxx`，其中`xxxxxx`是“第一步”中通过分享获得的软件包链接。

    ![输入图片说明](assets/abs-pr-006.png)

2. 推送PR。

    2.1 返回你的gitee个人仓库的根目录，并点击Pull Request，创建一个PR。

    ![image.png](./assets/abs-pr-009.png)

    2.2 目标分支选择 **anolis-challenge/activity-box-lab**。

    2.3 在PR的标题开头输入 **#IssueID** 模版关联任务。

    2.4 在PR正文描述的文本框中提交成果，第一行内容必须填写`abs_url=xxxxxx`，其中`xxxxxx`是“第一步”中通过分享获得的软件包链接。例如，`abs_url=https://abs.openanolis.cn/all_project/package/5567`。第二行开始，欢迎对本活动进行反馈，如体验中遇到的问题、体验后的感想、对ABS的建议等。

填写示例如下图所示。

![image.png](./assets/abs-pr-010.png)

请注意：
- 标题必须加上`IssuedID`。IssueID可以在Gitee任务页查看，如下图所示的位置。
    ![image.png](./assets/abs-task-001.png)

- PR正文的文本框中，首行必须包括`abs_url=<ABS任务链接>`。

- 修改的文件路径在ABS/\<gitee account name\>/目录下。

## 第三步：等待合入PR

提交PR后，社区人员会进行审核，请耐心等待。

- 如果收到回复，告知合入成功，则完成任务。

- 如果收到回复，告知需要修改，则合入PR失败。可能是PR不符合规范或其他原因，建议修改内容，再通过“/recheck”命令重新检查。

    ![image.png](./assets/abs-pr-011.png)

## 联系我们
关于本活动的常见问题，请参见<a href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%5B%E4%BA%BA%E4%BA%BA%E9%83%BD%E5%8F%AF%E4%BB%A5%E5%8F%82%E4%B8%8E%E5%BC%80%E6%BA%90%5D%E6%B4%BB%E5%8A%A8%E7%9A%84%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98(FAQ).md" target="_blank">活动FAQ</a>。

如果在做任务过程中，出现机器人无法正确合入PR的情况，请加入龙蜥社区的钉钉交流群，或infra SIG组钉钉群咨询。

![image.png](./assets/abs-pr-012.png)

![image.png](./assets/abs-pr-013.png)
