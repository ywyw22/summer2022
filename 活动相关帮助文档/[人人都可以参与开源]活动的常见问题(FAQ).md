#### 我已经是社区贡献者，可以参与活动领取奖品吗？
可以。本活动是为了真正实现「人人都可以参与开源」而推出的，无差别面向广大开发者。 

#### 活动会持续多久？
长期有效。不定期更新活动类型与任务，精彩不间断。

#### 如何认领任务？认领后，可以放弃并重新认领吗？
- 字字珠玑（捉虫/翻译）：提Issue后，会在Gitee页面中出现小龙的回复，即表示完成任务。经社区人员审核通过后，会发放贡献值。

- 随机试炼、龙蜥一刻的任务，在各个活动的<a target="_blank" href="https://openanolis.cn/community/activity">详情页</a>中，点击“领取任务”或“抽盲盒”按钮，按照弹窗提示进行认领。只要Gitee页面中出现小龙的回复，即表示认领成功。如果想放弃任务重新认领，则在Gitee任务页中输入“/abandon”，出现小龙的回复，即表示放弃当前任务。放弃后，可以重新认领任务。

- 字字珠玑（写文档）、一码当先的任务，在<a target="_blank" href="https://openanolis.cn/community/activity/gitee">活动详情页</a>中点击“领取任务”，前往Gitee选择一个任务，然后回复“/receive”。如果Gitee页面中出现小龙的回复，表示认领成功。如果想放弃任务重新认领，则在Gitee任务页中输入“/abandon”，出现小龙的回复，即表示放弃当前任务。放弃后，可以重新认领任务。

以上这些任务，经社区人员审核通过后，会发放贡献值。

如果想成为社区推广大使，或分享开发者故事，请参见<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%E5%A6%82%E4%BD%95%E6%88%90%E4%B8%BA%E7%A4%BE%E5%8C%BA%E6%8E%A8%E5%B9%BF%E5%A4%A7%E4%BD%BF.md">如何成为社区推广大使</a>和<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%E5%A6%82%E4%BD%95%E5%88%86%E4%BA%AB%E4%BD%A0%E7%9A%84%E5%BC%80%E5%8F%91%E8%80%85%E6%95%85%E4%BA%8B.md">如何分享你的开发者故事</a>。

#### 如何查看已经认领的任务，以及任务的状态？
在<a target="_blank" href="https://openanolis.cn/">龙蜥社区官网</a>右上角，点击账号头像，选择“我的贡献值”，查看已经认领的任务，以及任务的状态。

![查看任务](Image/%E6%9F%A5%E7%9C%8B%E4%BB%BB%E5%8A%A1.png)

#### 任务领取后有完成期限吗？
建议尽快完成，可以获得贡献值，优先兑换心仪的礼品、尽早兑换实习证明。

#### 如何提交任务？

- 捉虫与翻译任务，只需要在文档页面一键提Issue。成功提交Issue就表示完成了任务。

- 写文档、随机试炼的盲盒任务、龙蜥一刻、一码当先中的任务，请提交Pull Request（PR）。提交PR的目标仓库，可以在认领的任务详情中找到。

- 领取了社区推广大使、分享开发者故事等任务，详情请参见<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%E5%A6%82%E4%BD%95%E6%88%90%E4%B8%BA%E7%A4%BE%E5%8C%BA%E6%8E%A8%E5%B9%BF%E5%A4%A7%E4%BD%BF.md">如何成为社区推广大使</a>和<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%E5%A6%82%E4%BD%95%E5%88%86%E4%BA%AB%E4%BD%A0%E7%9A%84%E5%BC%80%E5%8F%91%E8%80%85%E6%95%85%E4%BA%8B.md">如何分享你的开发者故事</a>。

#### 如何提交Pull Request（PR）？
提交PR有快捷版和普通版的方式：

- （快捷版）如果你想简单快速提PR，即在网页上提交PR，请参照以下步骤。这些步骤以写文档任务为例。

    1. 前往<a target="_blank" href="https://gitee.com/anolis-challenge/activity-common-task">提交PR的仓库</a>。

        请注意，每一个任务要求提交的仓库可能会不同。我们已经为每一类任务提供了pr提交的指导书，详见任务页。

    1. 点击“文件”->“新建文件”
        ![输入图片说明](Image/pr-%E6%96%B0%E5%BB%BA%E6%96%87%E4%BB%B6.png)
    1. 输入你文档内容。请注意，一定要在“提交信息”中加上Issue ID，否则无法识别发放贡献值。
        ![输入图片说明](Image/pr-%E6%8F%90%E4%BA%A4pr.png)

- （普通版）如果你想在本地进行日常PR，需要下载Gitee客户端，然后在客户端上执行命令操作。

    详细的操作步骤可以参考<a target="_blank" href="https://gitee.com/anolis-challenge/summer2022/blob/master/%E9%9A%8F%E6%9C%BA%E8%AF%95%E7%82%BC/T-One%E6%B5%8B%E8%AF%95/T-One%E4%BB%BB%E5%8A%A1%E9%AA%8C%E6%94%B6%E6%B5%81%E7%A8%8B.md#%E6%8F%90pr%E6%B5%81%E7%A8%8B%E4%B8%A4%E7%A7%8D%E6%96%B9%E5%BC%8F%E4%BB%BB%E9%80%89%E5%85%B6%E4%B8%80">T-One测试任务中的如何提交PR步骤</a>。

#### 任务被拒绝怎么办？
如果提交的成果被拒绝，管理员会写明原因。你也可以添加助手小龙的微信，入群咨询。

#### 如何查看自己的贡献值？
在<a target="_blank" href="https://openanolis.cn/">龙蜥社区官网</a>右上角，点击账号头像，选择“我的贡献值”，查看已经获得的贡献值。

![查看贡献值](Image/%E6%9F%A5%E7%9C%8B%E4%BB%BB%E5%8A%A1.png)

#### 如何用贡献值兑换礼品？
在龙蜥社区官网右上角，点击账号头像，选择“我的贡献值”。找到贡献值旁边的“去兑换”链接，填写兑换申请。

#### 如何申请实习证书或实践证明？
申请入口将在9月开放，请关注龙蜥社区官网通知。

如果你有需要提早申请，请联系社区人员。

#### 参与人数是否有限制？
一个任务可以由多人认领。但是，同一个账号不能重复认领同一个任务，随机试炼任务除外。

#### 文档捉虫和文档翻译是否有提交次数限制？
没有。但同一个账号不能重复提交同一个文档缺陷，或者重复翻译同一段内容。

#### 以上FAQ无法解决问题，如何联系社区人员？
- 添加助手小龙的微信（微信号：openanolis_assis），入微信群咨询。
- 搜索钉钉号：33311793，入钉钉群咨询。