
# KeenTune 配置文件说明
配置文件主要用于各服务的配置操作，包括服务的ip、端口、算法选择、调优参数的选择等操作。安装服务时是按默认的配置文件安装的，安装好后如果需要修改配置直接在/etc/keentune/conf目录下找到对应服务的配置文件进行修改，修改完成后重启服务即可生效。（注：在源配置文件修改时，需要重新安装、启动服务才可生效，在/etc/keentune/conf目录下修改对应的配置文件时，只需重启服务就可生效）

#### keentund 配置文件：/etc/keentune/conf/keentuned.conf
```conf
[keentuned]
KEENTUNED_HOME = /etc/keentune
PORT = 9871
HEARTBEAT_TIME = 30

[brain]
BRAIN_IP = localhost
BRAIN_PORT = 9872
ALGORITHM = tpe

[target-group-1]
TARGET_IP = localhost
TARGET_PORT = 9873
PARAMETER = sysctl.json

[bench-group-1]
BENCH_SRC_IP = localhost
BENCH_DEST_IP = localhost
BENCH_SRC_PORT = 9874
BENCH_DEST_PORT = 9875
BASELINE_BENCH_ROUND = 2
TUNING_BENCH_ROUND = 1
RECHECK_BENCH_ROUND = 1
BENCH_CONFIG = bench_wrk_nginx_long.json

[dump]
DUMP_BASELINE_CONFIGURATION = false
DUMP_TUNING_CONFIGURATION = false
DUMP_BEST_CONFIGURATION = true
DUMP_HOME = /var/keentune

[sensitize]
ALGORITHM = random
BENCH_ROUND = 2

[log]
LOGFILE_LEVEL  = DEBUG
LOGFILE_NAME   = keentuned.log
LOGFILE_INTERVAL = 2
LOGFILE_BACKUP_COUNT = 14

[version]
VERSION_NUM = 1.1.0
```
+ brain.BRAIN_IP: keentune-brain组件部署ip
+ brain.ALGORITHM: brain中使用的tuning算法
+ target-group-1.TARGET_IP: keentune-target组件部署ip
+ target-group-1.PARAMETER: keentune-target中调整的参数域
+ bench-group-1.BENCH_SRC_IP: keentune-bench组件部署ip
+ bench-group-1.BENCH_DEST_IP: keentune-bench组件发压目标ip，一般是target ip
+ bench-group-1.BENCH_CONFIG: keentune-bench中运行的benchmark脚本配置