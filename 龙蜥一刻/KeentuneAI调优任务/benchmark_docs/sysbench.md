# sysbench安装使用手册
## 工具介绍
SysBench是一个跨平台且支持多线程的模块化基准测试工具，用于评估系统在运行高负载的数据库时相关核心参数的性能表现。使用SysBench是为了绕过复杂的数据库基准设置，甚至在没有安装数据库的前提下，快速了解数据库系统的性能。


## 安装命令
通过yum进行sysbench安装
yum install sysbench -y


## 参数选择和运行
重点参数解释：
--threads=N                     number of threads to use
--time=N                        limit for total execution time in seconds
--mysql-host=[LIST,...]          MySQL server host
--mysql-port=[LIST,...]          MySQL server port（默认为3306）
--mysql-user=STRING              MySQL user
--mysql-password=STRING          MySQL password []
--mysql-db=STRING                MySQL database name
更多详细参数 sysbench --help 查看

包含多种测试：oltp_write_only   oltp_read_only  oltp_read_write等
执行命令以oltp_write_only 测试举例：
```java
##准备数据 
sysbench oltp_write_only --db-ps-mode=auto --mysql-host=XX --mysql-port=XX --mysql-user=XX --mysql-password=XX --mysql-db=sysdb --tables=100 --table_size=40000 --time=300 --report-interval=1 --threads=16 prepare
 ##运行
sysbench oltp_write_only --db-ps-mode=auto --mysql-host=XX --mysql-port=XX --mysql-user=XX --mysql-password=XX --mysql-db=sysdb --tables=100 --table_size=40000 --time=300 --report-interval=1 --threads=16 run
##清理
sysbench oltp_write_only --db-ps-mode=auto --mysql-host=XX --mysql-port=XX --mysql-user=XX --mysql-password=XX --mysql-db=sysdb --tables=100 --table_size=40000 --time=300 --report-interval=1 --threads=16 cleanup
```


## 性能指标分析
结果输出入下图：  
![](assets/11.png)

结果重点关注如下值：
transactions:                     4283989 (14279.42 per sec.)   #总事务数 （每秒事务数）
queries:                             25703962 (85676.60 per sec.)  #平均每秒能执行多少次
avg:                                    1.12                                                #平均响应时间
95th percentile:                1.58                                                #95%响应时间
