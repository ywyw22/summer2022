# KeentuneAI调优任务提PR步骤
参照题目指导手册的描述完成实验并保存或编辑需要提交的文件  

## 提PR流程 
1. 首先到 anolis-challenge/activity-lab-KeentuneAI <a href='https://gitee.com/anolis-challenge/activity-lab-keentune-ai' target='_blank'>仓库</a>下点击 fork，把代码 fork 到自己的 gitee 仓库  
![](assets/01.png)  
2. 将fork之后的仓库通过`git clone`下载到本地  
3. 在个人仓库的`activity-lab-KeentuneAI`目录下创建一个以自己**gitee登陆名**命名的目录  
4. 在该目录下保存**题目要求的文件，和一个README.md文件保存其他必要信息**，并推送到个人gitee仓库
5. 在自己的 gitee 仓库上点击 Pull Request，创建一个PR，目标分支选择 `anolis-challenge/activity-lab-keentune-ai`，在PR的标题开头输入 #issueID 模版关联 issue， 然后点击提交，等待管理员审核  
![](assets/07.png) 