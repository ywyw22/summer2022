# KeenTune专家调优实践——IO高吞吐

&emsp;&emsp;内核参数的调整往往能够影响系统和应用的性能，但内核参数数量太多，针对不同的应用我们如何选择和调整参数值？KeenTune中内置了几个内核参数配置方案，
可以通过KeenTune设置到你的测试环境中，并在一定程度上提升应用的性能。在这个题目中，我们希望你使用KeenTune工具对测试环境进行优化，
并使用benchmark工具验证优化效果。

# 1. KeenTune安装和配置
## 1.1 KeenTune安装
&emsp;&emsp;KeenTune总共包含四个组件：keentuned，keentune-target，keentune-brain，keentune-bench，在专家调优实践任务中，我们只需要安装keentuned和keentune-target，我们需要*一台*虚拟机来完成这个任务，并在虚拟机中安装keentuned和keentune-target和benchmark工具<a href='../benchmark_docs/fio.md' target='_blank'>FIO</a>来验证优化效果。

&emsp;&emsp;我们可以选择使用YUM或者源码安装的方式来下载和安装KeenTune的两个组件，具体步骤请参考<a href='../keentune_docs/KeenTune_install.md' target='_blank'>《Keentune安装手册》</a>

# 2. benchmark工具准备
## 2.1. benchmark工具安装  
&emsp;&emsp;FIO 工具是一款用于测试硬件存储性能的辅助工具，兼具灵活性、可靠性从而从众多性能测试工具中脱颖而出。磁盘的 I/O 是衡量硬件性能的最重要的指标之一，而 FIO 工具通过模拟 I/O负载对存储介质进行压力测试，并将存储介质的 I/O 数据直观的呈现出来。

&emsp;&emsp;根据实际业务的场景，一般将 I/O 的表现分为四种场景，随机读、随机写、顺序读、顺序写。FIO 工具允许指定具体的应用模式，配合多线程对磁盘进行不同深度的测试。
FIO 工具已经集成在 AnolisOS 8.2/8.4的yum仓库中，可以直接获取安装。

&emsp;&emsp;FIO的具体的安装步骤请参考<a href='../benchmark_docs/fio.md' target='_blank'>《FIO安装使用手册》</a>

## 2.2. benchmark工具使用
使用KeenTune进行专家调优之前，我们手动执行一下FIO来看一下专家调优之前的性能指标，具体的执行方法和参考指标请同样参考<a href='../benchmark_docs/fio.md' target='_blank'>《FIO安装使用手册》</a>

# 3. 使用KeenTune进行专家调优
KeenTune中内置的优化方案有：  

+ io_high_throughput.conf（IO高吞吐）  
+ net_high_throuput.conf（网络高吞吐）  
+ net_low_latency.conf（网络低时延）  

在这里我们希望你选择`io_high_throughput.conf`进行专家调优，具体方法参考<a href='../keentune_docs/KeenTune_profile.md' target='_blank'>《KeenTune专家调优》</a>

# 4. 任务对比和结果提交
&emsp;&emsp;通过调优前后都执行一次benchmark来验证调优效果，希望你提交benchmark在调优前后的运行截图各一张如下所示的截图，并在pr内容中补充必要的注释提到我们指定的仓库
```sh
[root@localhost dev]fio -filename=/dev/sda1 -direct=1 -iodepth 1 -thread -rw=randrw -ioengine=psync -bs=16k -size=500M -numjobs=10 -runtime=10
 -group_reporting -name=mytest mytest: (g=0): rw=randrw, bs=16K-16K/16K-16K, ioengine=psync, iodepth=1
...
mytest: (g=0): rw=randrw, bs=16K-16K/16K-16K, ioengine=psync, iodepth=1
fio 2.0.7
Starting 10 threads
Jobs: 10 (f=10): [mmmmmmmmmm] [100.0% done] [1651K/1831K /s] [100 /111  iops] [eta 00m:00s]
mytest: (groupid=0, jobs=10): err= 0: pid=4075
  read : io=28976KB, bw=2854.6KB/s, iops=178 , runt= 10151msec
    clat (usec): min=49 , max=525390 , avg=35563.60, stdev=69691.20
     lat (usec): min=49 , max=525390 , avg=35563.72, stdev=69691.20
    clat percentiles (usec):
     |  1.00th=[   65],  5.00th=[   70], 10.00th=[   92], 20.00th=[  116],
     | 30.00th=[  137], 40.00th=[  151], 50.00th=[  175], 60.00th=[  286],
     | 70.00th=[14144], 80.00th=[69120], 90.00th=[138240], 95.00th=[197632],
     | 99.00th=[280576], 99.50th=[301056], 99.90th=[452608], 99.95th=[528384],
     | 99.99th=[528384]
    bw (KB/s)  : min=   16, max= 1440, per=12.25%, avg=349.55, stdev=236.44
  write: io=29616KB, bw=2917.6KB/s, iops=182 , runt= 10151msec
    clat (usec): min=64 , max=2030.4K, avg=19768.52, stdev=155468.56
     lat (usec): min=64 , max=2030.4K, avg=19768.86, stdev=155468.56
    clat percentiles (usec):
     |  1.00th=[   70],  5.00th=[   83], 10.00th=[   93], 20.00th=[  115],
     | 30.00th=[  131], 40.00th=[  141], 50.00th=[  151], 60.00th=[  161],
     | 70.00th=[  177], 80.00th=[  209], 90.00th=[  310], 95.00th=[  532],
     | 99.00th=[700416], 99.50th=[1056768], 99.90th=[2007040], 99.95th=[2023424],
     | 99.99th=[2023424]
    bw (KB/s)  : min=   21, max= 1392, per=11.98%, avg=349.49, stdev=253.50
    lat (usec) : 50=0.03%, 100=13.11%, 250=58.60%, 500=7.76%, 750=1.34%
    lat (usec) : 1000=0.38%
    lat (msec) : 2=0.87%, 4=0.74%, 10=0.38%, 20=1.37%, 50=3.63%
    lat (msec) : 100=2.57%, 250=6.80%, 500=1.64%, 750=0.33%, 1000=0.14%
    lat (msec) : 2000=0.27%, >=2000=0.05%
  cpu          : usr=0.04%, sys=1.52%, ctx=45844, majf=0, minf=798
  IO depths    : 1=100.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=1811/w=1851/d=0, short=r=0/w=0/d=0
 
Run status group 0 (all jobs):
   READ: io=28976KB, aggrb=2854KB/s, minb=2854KB/s, maxb=2854KB/s, mint=10151msec, maxt=10151msec
  WRITE: io=29616KB, aggrb=2917KB/s, minb=2917KB/s, maxb=2917KB/s, mint=10151msec, maxt=10151msec
 
Disk stats (read/write):
  sda: ios=1818/1846, merge=0/5, ticks=63776/33966, in_queue=104258, util=99.84%

```

提交流程参考<a href='../KeenTune%E4%BB%BB%E5%8A%A1%E9%AA%8C%E6%94%B6%E6%B5%81%E7%A8%8B.md' target='_blank'>《任务验收流程》</a>

---   

## 常见问题
+ yum源安装找不到组件
修改/etc/yum.repos.d目录下的文件，增加以下内容
```conf
[KeenTune]
baseurl=https://mirrors.openanolis.cn/anolis/8.6/Plus/$basearch/os
enabled=1
gpgkey=https://mirrors.openanolis.cn/anolis/RPM-GPG-KEY-ANOLIS
gpgcheck=0
```

+ **如何指定调优机器**  
修改文件`/etc/keentune/conf/keentuned.conf`下的配置项`target-group-1 -> TARGET_IP`和`bench-group-1 -> BENCH_DEST_IP`为调优机器的ip **(也是你安装nginx的机器)**