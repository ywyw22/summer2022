# KeenTune智能参数调优——HORD内核参数调优

&emsp;&emsp;内核参数的调整往往能够影响系统和应用的性能，但内核参数数量太多，针对不同的应用我们如何选择和调整参数值？智能调优在系统优化领域有着重要的应用，KeenTune也提供了参数智能调优的功能，并内置了多种参数优化算法，可以对操作系统内核参数和应用参数进行智能化调优，与专家调优方案不同，参数智能调优需要耗费的时间更长。

&emsp;&emsp;在这个题目中，我们希望你使用KeenTune对内核参数进行智能调优，提升Nginx的服务性能，并使用Benchmark工具验证调优效果。

# 1. KeenTune安装和配置
## 1.1. KeenTune安装
&emsp;&emsp;KeenTune总共包含四个组件：keentuned，keentune-target，keentune-brain，keentune-bench，在专家调优实践任务中，我们需要安装全部四个组件，我们需要两台虚拟机A、B来完成这个任务，其中A虚拟机中安装keentuned，keentune-brain，keentune-bench以及benchmark工具<a href='../benchmark_docs/wrk.md' target='_blank'>wrk</a>，B虚拟机安装keentune-target来进行参数设置。

&emsp;&emsp;我们可以选择使用YUM或者源码安装的方式来下载和安装KeenTune的组件，具体步骤请参考<a href='../keentune_docs/KeenTune_install.md' target='_blank'>《Keentune安装手册》</a>

## 1.2. KeenTune的配置和启动
&emsp;&emsp;启动KeenTune之前还需要对KeenTune进行简单的配置，我们通过手动编辑`/etc/keentune/conf/keentuned.conf`文件配置虚拟机的ip从而构建起测试集群，并且指定调整sysctl.json参数域。

```conf
[target-group-1]
TARGET_IP = [ip address of vm B]
TARGET_PORT = 9873
PARAMETER = sysctl.json

[bench-group-1]
BENCH_DEST_IP = [ip address of vm B]

[brain]
ALGORITHM = hord
```

##### 注意：修改keentuned的配置文件之后需要重启keentuned服务
```sh
systemctl restart keentuned
```

# 2. benchmark工具准备
## 2.1. benchmark工具安装
&emsp;&emsp;wrk 是一款针对 http 协议的基准测试工具，它能够在单机多核 CPU 的条件下，使用系统自带的高性能 I/O 机制，如 epoll，kqueue 等，通过多线程和事件模式，对目标机器（服务端）产生大量的负载。即wrk能够开启多个连接访问接口，看接口最多每秒可以承受多少连接。

&emsp;&emsp;我们需要你在虚拟机B上安装wrk，具体的安装步骤请参考<a href='../benchmark_docs/wrk.md' target='_blank'>《wrk安装使用手册》</a>

## 2.2. benchmark工具使用
&emsp;&emsp;使用KeenTune进行专家调优之前，我们手动执行一下wrk来看一下专家调优之前的性能指标，具体的执行方法和参考指标请同样参考<a href='../benchmark_docs/wrk.md' target='_blank'>《wrk安装使用手册》</a>

## 2.3. benchmark脚本准备
&emsp;&emsp;我们需要为wrk实现一个自动化脚本使keentune-bench能够自动运行benchmark工具，我们已经实现好了这个<a href='https://gitee.com/anolis/keentuned/blob/master/daemon/examples/benchmark/wrk/ack_nginx_http_long_base.py' target='_blank'>脚本</a>，并为其写好了<a href='https://gitee.com/anolis/keentuned/blob/master/daemon/examples/benchmark/wrk/bench_wrk_nginx_long.json' target='_blank'>配置文件</a>，你可以直接使用。可以看到，对于wrk工具，我们会关注Requests_sec, Transfer_sec, Latency_90和Latency_99四个指标。

## 2.4. benchmark配置
手动编辑`/etc/keentune/conf/keentuned.conf`文件
```conf
[bench-group-1]
BENCH_DEST_IP = [ip address of vm B]
...
```

# 3. Nginx服务端安装
&emsp;&emsp;Nginx是我们的优化目标，我们需要在虚拟机B上安装Nginx服务并启动，具体操作步骤见<a href='../benchmark_docs/Nginx.md' target='_blank'>《nginx安装配置手册》</a>

# 4. KeenTune智能调优
&emsp;&emsp;接下来我们可以用KeenTune进行智能调优了，keentune-brain会不断给出参数配置并由keentune-target设置到nginx运行的环境中，然后会拉起keentune-bench对性能进行评估，并作为反馈数据。当然以上流程都是KeenTune自动完成的，我们需要准备好以下文件(**以下文件已随KeenTune安装**)  

+ <a href='https://gitee.com/anolis/keentuned/blob/master/daemon/examples/benchmark/wrk/ack_nginx_http_long_base.py' target='_blank'>benchmark运行脚本</a>
+ <a href='https://gitee.com/anolis/keentuned/blob/master/daemon/examples/benchmark/wrk/bench_wrk_nginx_long.json' target='_blank'>benchmark配置文件</a>
+ <a href='https://gitee.com/anolis/keentuned/blob/master/daemon/examples/parameter/sysctl.json' target='_blank'>待调优内核参数列表</a>

&emsp;&emsp;有了这些文件之后，我们可以使用KeenTune选择HORD算法对内核参数发起一次参数智能调优，具体操作步骤见<a href='../keentune_docs/KeenTune_auto_tuning.md' target='_blank'>《KeenTune智能调优》</a>

# 5. 调优效果验证和提交
&emsp;&emsp;调优之后你可以在日志文件中找到[调优幅度信息和最优参数文件]，我们希望你将算法给出的**最优参数配置**上传到我们指定的仓库，并**备注性能优化情况**(benchmark metrics and promotion)
![](./assets/tuning_result.jpg) 

&emsp;&emsp;最优配置文件可以在`/var/keentune/parameter/xxxx/*.json`
```json
{
    "parameters": [
        {
            "domain": "nginx",
            "name": "gzip_min_length",
            "range": [1024,102400],
            "dtype": "int",
            "value": 3200,
            "base": ""
        },
        {
            "domain": "nginx",
            "name": "client_header_buffer_size",
            "options": ["2k","4k","8k","16k","32k","64k"],
            "dtype": "string",
            "value": "4k",
            "base": ""
        }
    ],
    "current_round": 64
}
```

提交流程参考<a href='../KeenTune%E4%BB%BB%E5%8A%A1%E9%AA%8C%E6%94%B6%E6%B5%81%E7%A8%8B.md' target='_blank'>《任务验收流程》</a>

---   

## 常见问题
+ yum源安装找不到组件
修改/etc/yum.repos.d目录下的文件，增加以下内容
```conf
[KeenTune]
baseurl=https://mirrors.openanolis.cn/anolis/8.6/Plus/$basearch/os
enabled=1
gpgkey=https://mirrors.openanolis.cn/anolis/RPM-GPG-KEY-ANOLIS
gpgcheck=0
```

+ **如何指定调优机器**  
修改文件`/etc/keentune/conf/keentuned.conf`下的配置项`target-group-1 -> TARGET_IP`和`bench-group-1 -> BENCH_DEST_IP`为调优机器的ip **(也是你安装nginx的机器)**

+ **如何选择使用的优化算法**  
修改文件`/etc/keentune/conf/keentuned.conf`下的配置项`brain -> ALGORITHM`, 你可以将其配置为`tpe`来选择TPE算法或者`hord`来选择HORD算法。然后**重启keentuned**。
```shell
systemctl restart keentuned
```

+ 我找不到我的任务日志文件了  
所有的日志文件都保存在`/var/log/keentune`路径下

+ 在哪能找到最终的调优幅度  
**如果你正确的完成了调优**，在日志的最后能找到`[BEST] Tuning impromvment`信息

+ 在哪能找到最优参数配置  
**如果你正确的完成了调优**，最优参数配置保存在路径`/var/keentune/parameter`

+ 如何选择进行优化的参数  
修改文件`/etc/keentune/conf/keentuned.conf`下的配置项`target-group-1 -> PARAMETER`, 如果配置为`sysctl.json`即为优化内核参数，配置为`nginx.json`即为优化nginx参数。你也可以尝试将其配置为`sysctl.json,nginx.json`同时调整内核参数和nginx参数。然后**重启keentuned**。
```shell
systemctl restart keentuned
```

+ 如何选择使用的benchmark  
修改文件`/etc/keentune/conf/keentuned.conf`下的配置项`bench-group-1 -> BENCH_CONFIG`, 配置为`bench_wrk_nginx_long.json`即可。然后**重启keentuned**。
```shell
systemctl restart keentuned
```