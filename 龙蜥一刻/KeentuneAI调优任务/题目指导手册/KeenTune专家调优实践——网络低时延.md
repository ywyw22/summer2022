# KeenTune专家调优实践——网络低时延

&emsp;&emsp;内核参数的调整往往能够影响系统和应用的性能，但内核参数数量太多，针对不同的应用我们如何选择和调整参数值？KeenTune中内置了几个内核参数配置方案，可以通过KeenTune设置到你的测试环境中，并在一定程度上提升应用的性能。在这个题目中，我们希望你使用KeenTune工具对测试环境进行优化，并使用benchmark工具验证优化效果。

# 1. KeenTune安装和配置
## 1.1 KeenTune安装
&emsp;&emsp;KeenTune总共包含四个组件：keentuned，keentune-target，keentune-brain，keentune-bench，在专家调优实践任务中，我们只需要安装keentuned和keentune-target，我们需要两台虚拟机A、B来完成这个任务，其中A虚拟机中安装keentuned和benchmark工具<a href='../benchmark_docs/wrk.md' target='_blank'>wrk</a>，B虚拟机安装keentune-target和nginx服务

&emsp;&emsp;我们可以选择使用YUM或者源码安装的方式来下载和安装KeenTune的两个组件，具体步骤请参考<a href='../keentune_docs/KeenTune_install.md' target='_blank'>《Keentune安装手册》</a>

# 2. benchmark工具准备
## 2.1. benchmark工具安装
&emsp;&emsp;wrk 是一款针对 http 协议的基准测试工具，它能够在单机多核 CPU 的条件下，使用系统自带的高性能 I/O 机制，如 epoll，kqueue 等，通过多线程和事件模式，对目标机器（服务端）产生大量的负载。即wrk能够开启多个连接访问接口，看接口最多每秒可以承受多少连接。

&emsp;&emsp;所以我们希望你在虚拟机A上安装wrk，具体的安装步骤请参考<a href='../benchmark_docs/wrk.md' target='_blank'>《wrk安装使用手册》</a>

## 2.3 Nginx服务端安装
&emsp;&emsp;Nginx是我们的优化目标，我们需要在虚拟机B上安装Nginx服务并启动，具体操作步骤见<a href='../benchmark_docs/Nginx.md' target='_blank'>《nginx安装配置手册》</a>

## 2.2. benchmark工具使用
&emsp;&emsp;使用KeenTune进行专家调优之前，我们手动执行一下wrk来看一下专家调优之前的性能指标，具体的执行方法和参考指标请同样参考<a href='../benchmark_docs/wrk.md' target='_blank'>《wrk安装使用手册》</a>

# 3. 使用KeenTune进行专家调优
KeenTune中内置的优化方案有：  

+ io_high_throughput.conf（IO高吞吐）    
+ net_high_throuput.conf（网络高吞吐）    
+ net_low_latency.conf（网络低时延）   

在这里我们希望你选择`net_low_latency.conf`进行专家调优，具体方法参考<a href='../keentune_docs/KeenTune_profile.md' target='_blank'>《KeenTune专家调优》</a>

# 4. 任务对比和结果提交
&emsp;&emsp;通过调优前后都执行一次benchmark来验证调优效果，希望你提交benchmark在调优前后的运行截图各一张如下所示的截图，并在pr内容中补充必要的注释提到我们指定的仓库
![](./assets/profile.jpg) 

提交流程参考<a href='../KeenTune%E4%BB%BB%E5%8A%A1%E9%AA%8C%E6%94%B6%E6%B5%81%E7%A8%8B.md' target='_blank'>《任务验收流程》</a>

---   

## 常见问题
+ yum源安装找不到组件
修改/etc/yum.repos.d目录下的文件，增加以下内容
```conf
[KeenTune]
baseurl=https://mirrors.openanolis.cn/anolis/8.6/Plus/$basearch/os
enabled=1
gpgkey=https://mirrors.openanolis.cn/anolis/RPM-GPG-KEY-ANOLIS
gpgcheck=0
```

+ **如何指定调优机器**  
修改文件`/etc/keentune/conf/keentuned.conf`下的配置项`target-group-1 -> TARGET_IP`和`bench-group-1 -> BENCH_DEST_IP`为调优机器的ip **(也是你安装nginx的机器)**