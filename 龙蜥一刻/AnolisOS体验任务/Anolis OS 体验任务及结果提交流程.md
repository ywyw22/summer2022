## 下载体验Anolis OS

在龙蜥社区官网活动页领取任务后，系统会自动创建一个任务（Issue），前往 Gitee 任务页查看任务内容，开始体验龙蜥操作系统（Anolis OS）！

### 准备工作

开始体验前，请在Gitee任务页找到并记下Issue ID（即任务ID），后续在提交任务成果PR时，需要用到此Issue ID。

### 第一步：安装虚拟机软件

本示例中，安装的是VMware Workstation Player。安装步骤请参考详细的<a href='https://openanolis.cn/video/593034376175730689' target='_blank'>视频指导-安装虚拟机</a>。

### 第二步：下载Anolis OS镜像

打开<a href='https://openanolis.cn/download' target='_blank'>龙蜥操作系统的下载页面</a>下载镜像。本示例中，下载的是Anolis OS 8.4 GA版本。

下载需要一点时间，请耐心等待。

### 第三步：安装Anolis OS

本示例中，安装的是Anolis OS 8.4版本。安装步骤请参考详细的<a href='https://openanolis.cn/video/593035250721669122' target='_blank'>视频指导-安装Anolis OS</a>。

### 第四步：体验硬件兼容性工具Ancert

请观看<a href='https://openanolis.cn/video/593036054878502913' target='_blank'>视频指导-体验Ancert</a>，或阅读<a href='https://openanolis.cn/sig/HCT/doc/515463617816101039' target='_blank'>使用手册</a>，完成体验。

### 第五步：提交任务日志

完成硬件兼容性体验以后，需要将日志文件提交到任务仓库。

#### 1. 准备PR。
  
1.1 前往<a href='https://gitee.com/anolis-challenge/activity-lab-anolis-os' target='_blank'>anolis-challenge / activity-lab-AnolisOS仓库</a>，下点击fork，把官方仓库的代码fork到你个人的gitee仓库中。

![image.png](./asserts/fork-repo.png)

1.2 在个人空间的activity-lab-anolis-os仓库的ancert文件夹下创建一个子文件夹，该文件夹以你的gitee用户名来命名。

![image.png](./asserts/create-folder.png)

1.3 在上述个人目录中上传“**第四步**”中得到的ancert任务日志的打包文件xxx.tar。

![image.png](./asserts/upload-tar-1.png)

![image.png](./asserts/upload-tar-2.png)

#### 2. 推送PR。

2.1 返回你的gitee个人仓库的根目录，并点击Pull Request，创建一个PR。

![image.png](./asserts/create-pr-1.png)

2.2 目标分支选择 anolis-challenge / activity-lab-AnolisOS 仓库的master分支。

2.3 在PR的标题开头输入 **#IssueID** 模版关联任务。

2.4 在PR正文描述的文本框中提交成果，第一行内容必须需要包含ancert_file=xxx.tar，其中xxx.tar是刚才上传的ancert任务日志的打包文件，例如，ancert_file=list_hardware_2022-06-23_16-01-35.tar。第二行开始，欢迎对本活动进行反馈，如体验中遇到的问题、体验后的感想、对Anolis OS安装使用/ancert工具的建议等。填写示例如下图所示

![image.png](./asserts/create-pr-2.png)

请注意：

- 标题必须加上`IssuedID`。IssueID可以在**Gitee任务页**查看

- PR正文的文本框中，首行必须包括`ancert_file=xxx.tar`

- 上传的tar包文件路径在`ancert/<gitee account name>/`目录下。

#### 3. 等待合入PR。
提交PR后，社区人员会进行审核，请耐心等待。

- 如果收到回复，告知合入成功，则完成任务。

- 如果收到回复，告知需要修改，则合入PR失败。可能是PR不符合规范或其他原因，建议修改内容，再通过“/recheck”命令重新检查。

### 联系我们
关于本活动的常见问题，请参见<a href='https://gitee.com/anolis-challenge/summer2022/blob/master/%E6%B4%BB%E5%8A%A8%E7%9B%B8%E5%85%B3%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3/%5B%E4%BA%BA%E4%BA%BA%E9%83%BD%E5%8F%AF%E4%BB%A5%E5%8F%82%E4%B8%8E%E5%BC%80%E6%BA%90%5D%E6%B4%BB%E5%8A%A8%E7%9A%84%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98(FAQ).md' target='_blank'>活动FAQ</a>。

如果在做任务过程中，出现机器人无法正确合入PR的情况，请加入龙蜥社区的钉钉交流群，或软硬件兼容性 SIG组钉钉群咨询。

![image.png](./asserts/sig-2dcode-1.png)

![image.png](./asserts/sig-2dcode-2.png)